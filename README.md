# 667 Client Deob

---

**This was released on [Rune-Server](https://www.rune-server.ee/runescape-development/rs-503-client-server/downloads/525214-667-deob.html) by [.alicia](https://www.rune-server.ee/members/.alicia/).**


---

> Found this on the back ends of the internet, so I'm re-releasing it.
> 
> Don't know who's it is, so I can't give credits to anyone for deobbing it. (I assume Discardedx2?)
> ![Image Preview](https://cdn.discordapp.com/attachments/557554114553184257/937708548505370634/unknown.png)
>
> Link: [667 deob](https://www.mediafire.com/download/gc5xdoqdidhsjyc/667+deob.zip)